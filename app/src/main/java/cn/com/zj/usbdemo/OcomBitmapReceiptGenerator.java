package cn.com.zj.usbdemo;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;

public class OcomBitmapReceiptGenerator {

    public static void main(String[] args) {

        String itemName = "Coffee";
        String quantity = "1";
        String itemPrice = "100.00";
        String taxes = "V";
        int fontSize = 25;
        int widthQty = 76;
        int widthName = 350;
        int widthPrice = 150;

        OcomBitmapReceiptGenerator ocomBitmapReceiptGenerator = new OcomBitmapReceiptGenerator();

        // this is one order line to be printed for example.
        Bitmap orderLine1 = ocomBitmapReceiptGenerator.rowItemBitmap(
                itemName,
                quantity,
                itemPrice,
                taxes,
                fontSize,
                widthQty,
                widthName,
                widthPrice
        );

        System.out.print(orderLine1);
    }

    public Bitmap rowItemBitmap(String itemName, String quantity, String itemPrice, String taxes, int fontSize, int widthQty, int widthName, int widthPrice) {
        return mergeBitmap(
                mergeBitmap(
                        stringToBitmap(quantity, fontSize, Typeface.NORMAL, Layout.Alignment.ALIGN_NORMAL, widthQty),
                        stringToBitmap(itemName, fontSize, Typeface.NORMAL, Layout.Alignment.ALIGN_NORMAL, widthName)
                ),
                stringToBitmap(itemPrice + taxes, fontSize, Typeface.NORMAL, Layout.Alignment.ALIGN_OPPOSITE, widthPrice)
        );
    }

    private Bitmap stringToBitmap(String printText, int textSize, int style, Layout.Alignment alignment, int widthImage) {
        Canvas canvas;
        Paint paint = new Paint();

        paint.setTypeface(Typeface.create(Typeface.MONOSPACE, style));
        paint.setTextSize((float) textSize);

        paint.getTextBounds(printText, 0, printText.length(), new Rect());

        TextPaint textpaint = new TextPaint(paint);

        StaticLayout staticLayout = new StaticLayout(printText, textpaint, widthImage, alignment, 1f, 0f, false);
        Bitmap bitmap = Bitmap.createBitmap(staticLayout.getWidth(), staticLayout.getHeight() + 2, Bitmap.Config.ARGB_8888);

        // Create canvas
        canvas = new Canvas(bitmap);
        canvas.drawColor(Color.WHITE);
        canvas.translate(0f, 0f);
        staticLayout.draw(canvas);
        return bitmap;
    }

    private Bitmap mergeBitmap(Bitmap fr, Bitmap sc) {

        Bitmap comboBitmap;

        int width = fr.getWidth() + sc.getWidth();
        int height;

        height = fr.getHeight();

        if (sc.getHeight() > fr.getHeight()) {
            height = sc.getHeight();
        }

        comboBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas comboImage = new Canvas(comboBitmap);

        comboImage.drawBitmap(fr, 0f, 0f, null);
        comboImage.drawBitmap(sc, (float) fr.getWidth(), 0f, null);
        return comboBitmap;
    }
}

